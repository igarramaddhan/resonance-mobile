import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppNavigation from './src/AppNavigation';
import NavigationService from './src/libs/NavigationService';

export default function App() {
  return (
    <AppNavigation ref={ref => NavigationService.setTopLevelNavigator(ref)} />
  );
}
