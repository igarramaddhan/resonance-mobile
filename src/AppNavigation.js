import React from 'react';
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
} from 'react-navigation';
import Home from './screens/Home';
import Drawer from './components/Drawer';
import StoryDetail from './screens/StoryDetail';

const AppStack = createStackNavigator(
  {
    Home: Home,
    StoryDetail: StoryDetail,
  },
  {
    headerMode: 'none',
  }
);

const AppDrawer = createDrawerNavigator(
  {
    AppStack: AppStack,
  },
  {
    contentComponent: props => <Drawer {...props} />,
  }
);

export default createAppContainer(AppDrawer);
