import { Platform, StatusBar } from 'react-native';

export const IS_IOS = Platform.OS === 'ios';
export const PADDING_TOP = IS_IOS ? 0 : StatusBar.currentHeight;
export const HEADER_HEIGHT = IS_IOS ? 40 : 56;
