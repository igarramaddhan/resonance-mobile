// @flow
import {
  DrawerActions,
  NavigationParams,
  NavigationActions,
  NavigationContainerComponent,
} from 'react-navigation';

let _navigator: NavigationContainerComponent;

function setTopLevelNavigator(navigatorRef: NavigationContainerComponent) {
  _navigator = navigatorRef;
}

function navigate(routeName: string, params: NavigationParams) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function back() {
  _navigator.dispatch(NavigationActions.back());
}

function getNavigator() {
  return _navigator;
}

function toggleDrawer() {
  _navigator.dispatch(DrawerActions.toggleDrawer());
}

export default {
  back,
  navigate,
  toggleDrawer,
  getNavigator,
  setTopLevelNavigator,
};
