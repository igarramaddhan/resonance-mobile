// @flow
import React from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { PADDING_TOP } from '../libs/metrics';
import Header from '../components/Header';
import Highlights from '../components/Highlights';
import StoryCard from '../components/StoryCard';

const Home = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Header />
      <ScrollView style={{ flex: 1 }}>
        <Highlights />
        <FlatList
          scrollEnabled={false}
          data={['', '', '', '', '', '', '', '', '', '', '', '']}
          style={{ padding: 16 }}
          ListHeaderComponent={() => (
            <Text style={styles.trendingTitle}>Trending</Text>
          )}
          renderItem={({ item }) => <StoryCard />}
          keyExtractor={(_, index) => `${index}`}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: PADDING_TOP,
  },
  trendingTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 8,
  },
});

export default Home;
