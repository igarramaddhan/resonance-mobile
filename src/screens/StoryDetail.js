// @flow
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { PADDING_TOP } from '../libs/metrics';
import Header from '../components/Header';
import Highlights from '../components/Highlights';

const StoryDetail = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Header />
      <ScrollView style={{ flex: 1 }}>
        <Highlights />
        <Text style={styles.tagText}>Business</Text>
        <Text style={styles.contentTitle}>
          Tiang Beton Tol Bogor Outer Ring Road Ambruk
        </Text>
        <View style={styles.imageContainer}>
          <Image
            style={{ flex: 1, height: undefined, width: undefined }}
            source={{
              uri:
                'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1562724854/b5sz5kjkqnxg1malzmip.jpg',
            }}
          />
        </View>
        <Text style={styles.contentText}>
          Sebuah tiang pancang beton proyek Tol Bogor Outer Ring Road (BORR)
          seksi IIIA (Simpang Yasmin-Simpang Semplak) ambruk. PT Marga Sarana
          Jabar (MSJ) selaku pelaksana proyek pembangunan Tol BORR seksi IIIA
          sepanjang 2,85 kilometer memastikan tidak ada korban jiwa dari
          kejadian ini.
        </Text>
        <Text style={styles.contentText}>
          "Jatuhnya beton di pier head 109 terjadi pada Rabu, 10 Juli 2019 jam
          05.15 WIB. Tidak ada korban jiwa hanya 2 orang luka ringan dan sudah
          dirawat di RS Hermina," ungkap Direktur Utama PT MSJ Hendro Atmodjo di
          lokasi, Rabu (10/7).
        </Text>
        <Text style={styles.contentText}>
          Hendro menjelaskan ambruknya salah satu beton BORR disebabkan karena
          masalah teknis. Balok penyangga disebut tidak mampu menahan coran.
        </Text>
        <View style={styles.imageContainer}>
          <Image
            style={{ flex: 1, height: undefined, width: undefined }}
            source={{
              uri:
                'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1562724854/mt7vevwewwo5vo04mtg1.jpg',
            }}
          />
        </View>
        <Text style={styles.contentText}>
          "Saat dicor ke (tiang beton) 22 terjadi ketidakkuatan dari balok
          penyangga yang menyebabkan jatuhnya beton yang ada di dalam pier head,
          dan (coran) tumpah ke jalan arteri di bawahnya," sebutnya.
        </Text>
        <Text style={styles.contentText}>
          Atas kejadian ini, MSJ menghentikan sementara proyek di lokasi.
          Pihaknya akan menunggu investigasi yang dilakukan Komite Keselamatan
          Konstruksi dari Kementerian Pekerjaan Umum dan Perumahan Rakyat
          (PUPR).
        </Text>
        <Text style={styles.contentText}>
          "Kami sudah perintahkan untuk menghentikan pengerjaan proyek untuk
          hari ini sampai nanti ada clearance dari Komite Keselamatan Konstruksi
          untuk selanjutnya bisa dilanjutkan atau tidak atau ada evaluasi dari
          proyek dan pelaksanaannya," tuturnya.
        </Text>
        <Text style={styles.contentText}>
          Tidak hanya itu, pihaknya saat ini sedang berkoordinasi dengan
          Kepolisian untuk menangani kemacetan di jalur arteri, tepat di samping
          pengerjaan proyek. "Agar lalu lintas di arteri bisa lancar," ucapnya.
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: PADDING_TOP,
  },
  trendingTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  tagText: {
    color: '#5999E4',
    fontSize: 19,
    paddingHorizontal: 16,
  },
  contentTitle: {
    fontSize: 38,
    marginBottom: 16,
    fontWeight: 'bold',
    paddingHorizontal: 16,
  },
  contentText: {
    fontSize: 18,
    marginVertical: 8,
    paddingHorizontal: 16,
  },
  imageContainer: {
    height: 230,
  },
});

export default StoryDetail;
