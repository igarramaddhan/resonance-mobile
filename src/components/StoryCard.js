// @flow
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import { Feather } from '@expo/vector-icons';
import NavigationService from '../libs/NavigationService';

const StoryCard = () => {
  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <TouchableOpacity
          onPress={() => NavigationService.navigate('StoryDetail')}
        >
          <Text style={styles.title}>
            Tiang Beton Tol Bogor Outer Ring Road Ambruk
          </Text>
        </TouchableOpacity>
        <View style={styles.authorContainer}>
          <View
            style={{
              width: 25,
              height: 25,
              backgroundColor: 'grey',
              borderRadius: 12.5,
              marginRight: 8,
            }}
          />
          <Text style={styles.authorText}>kumparanBISNIS</Text>
        </View>
        <View style={styles.iconsContainer}>
          <Feather name="heart" size={25} style={styles.iconStyle} />
          <Feather name="message-circle" size={25} style={styles.iconStyle} />
          <Feather name="share-2" size={25} style={styles.iconStyle} />
          <Text>sehari</Text>
        </View>
      </View>
      <View style={styles.imageContainer}>
        <Image
          style={{ flex: 1, height: undefined, width: undefined }}
          source={{
            uri:
              'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1562724854/b5sz5kjkqnxg1malzmip.jpg',
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 4,
    borderTopColor: '#eee',
    borderTopWidth: 1,
    paddingVertical: 16,
  },
  contentContainer: {
    flex: 1,
  },
  title: {
    fontSize: 19,
    fontWeight: 'bold',
  },
  authorContainer: {
    marginVertical: 8,
    flexDirection: 'row',
  },
  authroImage: {},
  authorText: {
    fontSize: 15,
    color: 'rgba(0,0,0,0.6)',
  },
  iconsContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconStyle: {
    marginRight: 8,
  },
  imageContainer: {
    width: 103,
    height: 103,
    marginRight: 16,
  },
});

export default StoryCard;
