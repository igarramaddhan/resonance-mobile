// @flow
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  // $FlowFixMe
  StyleProp,
  // $FlowFixMe
  ViewStyle,
  // $FlowFixMe
  TextStyle,
} from 'react-native';
import { DrawerItemsProps } from 'react-navigation';
import { PADDING_TOP, HEADER_HEIGHT } from '../libs/metrics';

const LoginButton = (props: {
  text: string,
  containerStyle?: StyleProp<ViewStyle>,
  textStyle?: StyleProp<TextStyle>,
}) => {
  return (
    <TouchableOpacity
      style={[styles.buttonContainer, { ...props.containerStyle }]}
    >
      <Text style={[styles.buttonText, { ...props.textStyle }]}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

// $FlowFixMe
const Drawer = (props: React.PropsWithChildren<DrawerItemsProps>) => {
  return (
    <View style={styles.container}>
      <View style={styles.topContent}>
        <Text style={styles.topContentText}>Menu</Text>
      </View>
      <View style={styles.loginContainer}>
        <Text style={styles.loginDescription}>Masuk kumparan menggunakan</Text>
        <LoginButton
          text="Google"
          containerStyle={{ backgroundColor: '#DB4437' }}
          textStyle={{ color: 'white' }}
        />
        <LoginButton
          text="Facebook"
          containerStyle={{ backgroundColor: '#3c5a99' }}
          textStyle={{ color: 'white' }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: PADDING_TOP,
  },
  topContent: {
    height: HEADER_HEIGHT,
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  topContentText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  loginContainer: {
    padding: 16,
  },
  loginDescription: {
    fontSize: 16,
    color: 'black',
    alignSelf: 'center',
    marginVertical: 16,
  },
  buttonContainer: {
    margin: 2,
    height: 40,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DDDDDD',
  },
  buttonText: {
    color: 'black',
    fontWeight: 'bold',
  },
});

export default Drawer;
