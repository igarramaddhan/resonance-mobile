// @flow
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from 'react-native';

const ItemSeparator = () => <View style={{ width: 16, height: 44 }} />;

const Highlights = () => {
  return (
    <FlatList
      horizontal
      style={{ backgroundColor: '#FAFAFA', height: 44 }}
      showsHorizontalScrollIndicator={false}
      data={[
        'Serbuan Sampah Impor',
        'Bambang Mencari Ibunya',
        'Bambang Dicariin Ibunya',
        'Siapa yang Mencari Bambang',
      ]}
      renderItem={({ item }) => (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}
        >
          <View
            style={{
              backgroundColor: '#D14E4E',
              width: 8,
              height: 8,
              borderRadius: 4,
              marginRight: 4,
            }}
          />
          <Text>{item}</Text>
        </View>
      )}
      ListHeaderComponent={() => <ItemSeparator />}
      ListFooterComponent={() => <ItemSeparator />}
      ItemSeparatorComponent={() => <ItemSeparator />}
      keyExtractor={(_, index) => `${index}`}
    />
  );
};

const styles = StyleSheet.create({});

export default Highlights;
