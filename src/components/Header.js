// @flow
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { IS_IOS, HEADER_HEIGHT } from '../libs/metrics';
import { Feather } from '@expo/vector-icons';
import {} from 'react-navigation';
import NavigationService from '../libs/NavigationService';

const Header = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.iconContainer}
        onPress={() => NavigationService.toggleDrawer()}
      >
        <Feather name="menu" size={25} />
      </TouchableOpacity>
      <View style={styles.mainTitle}>
        <Image source={require('../../assets/logo.png')} />
      </View>
      <TouchableOpacity style={styles.iconContainer}>
        <Feather name="search" size={25} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: HEADER_HEIGHT,
    backgroundColor: 'white',
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  mainTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    width: HEADER_HEIGHT,
    height: HEADER_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Header;
